CC = clang
CFLAGS = -g
OBJ = dnsq.c

%.o: %.c
	$(CC) -c -o $@ $<

dnsq: $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^